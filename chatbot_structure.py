from addFunctions import *
from deleteFunctions import *
from updateFunctions import *
from service_functions.get_functions import *
from service_functions.keyboard import *
from service_functions.check_functions import *
from bunch import *


def nextNode(location, message, connect, vk_id=None):
    """
    Функция, отвечающая за реакции на сообщения пользователя
    :param location: Местоположение пользователя
    :param message: Сообщение пользователя
    :param connect: Соединениие с БД
    :param vk_id: ID пользователя ВК
    :return: Следующее месторасположение пользователя
    """
    # Выделить тип месторасположения
    loc_type = location[:3]
    # Если тип относится к проектам
    if loc_type[:1] == "1":
        # Если тип проекта относится к дополнительным задачам
        if loc_type == "150":
            # Действия, при сообщении
            if message.lower() == "создать":
                next_loc = nextNodeProject("100")
            elif message.lower() == "выбрать":
                # Записать новое местоположение
                next_loc = nextNodeProject("151")
            elif message.lower() == "назад":
                # Записать новое местоположение
                next_loc = mainMenu()
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            elif message.lower() == "удалить":
                next_loc = nextNodeProject("120")
            else:
                print("Error in nextNode150")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип проекта относится к дополнительным задачам
        if loc_type == "151":
            # Действия, при сообщении
            if message.lower() == "назад":
                # Записать новое местоположение
                next_loc = "1500"
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            # Проверить является ли сообщение реакцией на нажатии соотвествующей кнопки на клавиатуре
            elif checkMoveButton(message, vk_id, connect) is True:
                # Присвоить новому местоположение текущее
                next_loc = location
            # Проверить подходит ли сообщение под форму названий элементов
            elif checkName(message.lower()) is True:
                # Если такой элемент в проектах присутствует
                if checkElementProject(message, vk_id, connect) is True and messageCut(message)[0] != "aylb1gq7":
                    # Выделить ID проекта
                    id_project = messageCut(message)[1]
                    # Выделить название проекта
                    message = messageCut(message)[0]
                    # Подготовить SQL запрос на вывод значений для БД
                    selectDb = """select * from test_table_projects where project_name = %s and vk_user = %s and id = %s"""
                    try:
                        with connect.cursor() as cursor:
                            # Передаем SQL запрос БД
                            cursor.execute(selectDb, (message, vk_id, id_project))
                            # Передать переменной ответ от БД
                            project = cursor.fetchall()
                            # Если элементы, удовлетворяющие запросу поиска существуют
                            if project is not None:
                                # Ищем Id необходимого проекта
                                id_project = project[0]["id"]
                                # Записать новое местоположение
                                next_loc = nextNodeProject("130", str(id_project))
                            else:
                                print("Error in nextNodeSearch151")
                                # При ошибке указать старое местоположение
                                next_loc = location
                    except:
                        print("Error in nextNode151")
                        # При ошибке указать старое местоположение
                        next_loc = location
                else:
                    print("Error in nextNode151")
                    # При ошибке указать старое местоположение
                    next_loc = location
            else:
                print("Error in nextNode151")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип проекта относится к изменению проекта
        if loc_type == "130":
            # Действия, при сообщении
            if message.lower() == "переименовать":
                next_loc = nextNodeProject("131", location[3:])
            elif message.lower() == "перейти к спискам":
                next_loc = nextNodeProject("250", location[3:])
            elif message.lower() == "удалить":
                delete_project(location[3:], connect)
                next_loc = "1510"
            elif message.lower() == "назад":
                # Записать новое местоположение
                next_loc = "1510"
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип относится к созданию проектов
        if loc_type == "100":
            # Реакция на сообщения
            if message.lower() == "назад":
                next_loc = goBack(vk_id, connect, location)
            elif message.lower() == "меню":
                next_loc = mainMenu()
            # Если пытаются создать нулевой проект
            elif message.lower() == "aylb1gq7":
                next_loc = location
            else:
                # Добавить проект в БД
                add_project(setLastID(connect), message, vk_id, connect)
                # Присвоить новое местоположение
                next_loc = nextNodeProject("150")
        # Если тип относится к удалению проектов
        if loc_type == "120":
            # Реакция на сообщение
            if message.lower() == "назад":
                # Присвоить новое местоположение
                next_loc = "1500"
            elif message.lower() == "меню":
                # Присвоить новое местоположение
                next_loc = mainMenu()
            # Проверить является ли сообщение реакцией на нажатии соотвествующей кнопки на клавиатуре
            elif checkMoveButton(message, vk_id, connect) is True:
                # Присвоить новому местоположение текущее
                next_loc = location
            # Проверить подходит ли сообщение под форму названий элементов
            elif checkName(message.lower()) is True:
                # Если такой элемент в проектах присутствует
                if checkElementProject(message, vk_id, connect) is True and messageCut(message)[0] != "aylb1gq7":
                    # Удалить проект
                    delete_project(messageCut(message)[1], connect)
                    # Присвоить новому местоположению текущее местоположение
                    next_loc = location
                else:
                    print("Error in nextNode")
                    # При ошибке указать старое местоположение
                    next_loc = location
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если узел относится к изменению проекта
        if loc_type == "131":
            # Реакция на сообщения
            if message.lower() == "назад":
                next_loc = goBack(vk_id, connect, location)
            elif message.lower() == "меню":
                next_loc = mainMenu()
            # Если пытаются переименовать нулевой проект
            elif message.lower() == "aylb1gq7":
                next_loc = location
            else:
                # Обновить имя проекта
                update_name_project(location[3:], message, connect)
                # Присвоить новое местоположение
                next_loc = nextNodeProject("130", location[3:])

    # Если тип относится к спискам
    if loc_type[:1] == "2":
        # Если тип списка относится к дополнительным задачам
        if loc_type == "250":
            if message.lower() == "назад":
                if int(location[3:]) == get_null_project(vk_id, connect):
                    # Записать новое местоположение
                    next_loc = mainMenu()
                else:
                    next_loc = nextNodeProject("130", location[3:])
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            elif message.lower() == "выбрать":
                # Записать новое местоположение
                next_loc = nextNodeProject("251", location[3:])
            elif message.lower() == "создать":
                # Записать новое местоположение
                next_loc = nextNodeProject("200", location[3:])
            elif message.lower() == "удалить":
                # Записать новое местоположение
                next_loc = nextNodeProject("220", location[3:])
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип относится к созданию списков
        if loc_type == "200":
                # Реакция на сообщения
                if message.lower() == "назад":
                    next_loc = goBack(vk_id, connect, location)
                elif message.lower() == "меню":
                    next_loc = mainMenu()
                else:
                    # Добавить проект в БД
                    add_list(setLastID(connect), message, location[3:], connect=connect)
                    # Присвоить новое местоположение
                    next_loc = nextNodeProject("250", location[3:])
        # Если тип относится к удалению списков
        if loc_type == "220":
            # Реакция на сообщение
            if message.lower() == "назад":
                # Присвоить новое местоположение
                next_loc = nextNodeProject("250", location[3:])
            elif message.lower() == "меню":
                # Присвоить новое местоположение
                next_loc = mainMenu()
            # Проверить является ли сообщение реакцией на нажатии соотвествующей кнопки на клавиатуре
            elif checkMoveButton(message, vk_id, connect) is True:
                # Присвоить новому местоположение текущее
                next_loc = location
            # Проверить подходит ли сообщение под форму названий элементов
            elif checkName(message.lower()) is True:
                # Если такой элемент в списках присутствует
                if checkElementList(message, location[3:], connect) is True:
                    # Удалить список
                    delete_list(messageCut(message)[1], connect)
                    # Присвоить новому местоположению текущее местоположение
                    next_loc = location
                else:
                    print("Error in nextNode")
                    # При ошибке указать старое местоположение
                    next_loc = location
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип узла для работы со списками относится к дополнительным задачам
        if loc_type == "251":
            # Действия, при сообщении
            if message.lower() == "назад":
                # Записать новое местоположение
                next_loc = nextNodeProject("250", location[3:])
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            # Проверить является ли сообщение реакцией на нажатии соотвествующей кнопки на клавиатуре
            elif checkMoveButton(message, vk_id, connect) is True:
                # Присвоить новому местоположение текущее
                next_loc = location
            # Проверить подходит ли сообщение под форму названий элементов
            elif checkName(message.lower()) is True:
                # Если такой элемент в списках присутствует
                if checkElementList(message, location[3:], connect) is True:
                    # Выделить ID списка
                    id_list = messageCut(message)[1]
                    # Выделить название списка
                    message = messageCut(message)[0]
                    # Подготовить SQL запрос на вывод значений для БД
                    selectDb = """select * from test_table_lists where list_name = %s and id_project = %s and id = %s"""
                    try:
                        with connect.cursor() as cursor:
                            # Передаем SQL запрос БД
                            cursor.execute(selectDb, (message, location[3:], id_list))
                            # Передать переменной ответ от БД
                            listlist = cursor.fetchall()
                            # Если элементы, удовлетворяющие запросу поиска существуют
                            if listlist is not None:
                                # Ищем Id необходимого списка
                                id_list = listlist[0]["id"]
                                # Записать новое местоположение
                                next_loc = nextNodeProject("230", str(id_list))
                            else:
                                print("Error in nextNodeSearch251")
                                # При ошибке указать старое местоположение
                                next_loc = location
                    except:
                        print("Error in nextNode251")
                        # При ошибке указать старое местоположение
                        next_loc = location
                else:
                    print("Error in nextNode251")
                    # При ошибке указать старое местоположение
                    next_loc = location
            else:
                print("Error in nextNode251")
                # При ошибке указать старое местоположение
                next_loc = location
        if loc_type == "230":
            # Действия, при сообщении
            if message.lower() == "переименовать":
                next_loc = nextNodeProject("231", location[3:])
            elif message.lower() == "удалить":
                next_loc = nextNodeProject("251", str(get_main_project(location[3:], connect)))
                delete_list(location[3:], connect)
            elif message.lower() == "перейти к задачам":
                next_loc = nextNodeProject("350", location[3:])
            elif message.lower() == "назад":
                # Записать новое местоположение
                next_loc = nextNodeProject("251", str(get_main_project(location[3:], connect)))
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если узел относится к изменению списка
        if loc_type == "231":
            # Реакция на сообщения
            if message.lower() == "назад":
                next_loc = goBack(vk_id, connect, location)
            elif message.lower() == "меню":
                next_loc = mainMenu()
            else:
                # Обновить имя списка
                update_name_list(location[3:], message, connect)
                # Присвоить новое местоположение
                next_loc = nextNodeProject("230", location[3:])
    # Если тип относится к задачам
    if loc_type[:1] == "3":
        # Если тип задачи относится к дополнительным задачам
        if loc_type == "350":
            if message.lower() == "назад":
                # Записать новое местоположение
                next_loc = nextNodeProject("230", location[3:])
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            elif message.lower() == "создать":
                # Записать новое местоположение
                next_loc = nextNodeProject("300", location[3:])
            elif message.lower() == "выбрать":
                # Записать новое местоположение
                next_loc = nextNodeProject("351", location[3:])
            elif message.lower() == "удалить":
                # Записать новое местоположение
                next_loc = nextNodeProject("320", location[3:])
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип относится к созданию задач
        if loc_type == "300":
            # Реакция на сообщения
            if message.lower() == "назад":
                next_loc = goBack(vk_id, connect, location)
            elif message.lower() == "меню":
                next_loc = mainMenu()
            else:
                # Добавить проект в БД
                task_id = setLastID(connect)
                add_task(task_id, message, location[3:], connect=connect)
                # Присвоить новое местоположение
                next_loc = nextNodeProject("301", str(task_id))
        # Если тип относится к удалению задач
        if loc_type == "320":
            # Реакция на сообщение
            if message.lower() == "назад":
                # Присвоить новое местоположение
                next_loc = nextNodeProject("350", location[3:])
            elif message.lower() == "меню":
                # Присвоить новое местоположение
                next_loc = mainMenu()
            # Проверить является ли сообщение реакцией на нажатии соотвествующей кнопки на клавиатуре
            elif checkMoveButton(message, vk_id, connect) is True:
                # Присвоить новому местоположение текущее
                next_loc = location
            # Проверить подходит ли сообщение под форму названий элементов
            elif checkName(message.lower()) is True:
                # Если такой элемент в задачах присутствует
                if checkElementTask(message, location[3:], connect) is True:
                    # Удалить задачу
                    delete_task(messageCut(message)[1], connect)
                    # Присвоить новому местоположению текущее местоположение
                    next_loc = location
                else:
                    print("Error in nextNode320")
                    # При ошибке указать старое местоположение
                    next_loc = location
            else:
                print("Error in nextNode320")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип узла для работы с задачами относится к дополнительным задачам
        if loc_type == "351":
            # Действия, при сообщении
            if message.lower() == "назад":
                # Записать новое местоположение
                next_loc = nextNodeProject("350", location[3:])
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            # Проверить является ли сообщение реакцией на нажатии соотвествующей кнопки на клавиатуре
            elif checkMoveButton(message, vk_id, connect) is True:
                # Присвоить новому местоположение текущее
                next_loc = location
            # Проверить подходит ли сообщение под форму названий элементов
            elif checkName(message.lower()) is True:
                # Если такой элемент в списках присутствует
                if checkElementTask(message, location[3:], connect) is True:
                    # Выделить ID списка
                    id_task = messageCut(message)[1]
                    # Выделить название списка
                    message = messageCut(message)[0]
                    # Подготовить SQL запрос на вывод значений для БД
                    selectDb = """select * from test_table_tasks where task_name = %s and id_list = %s and id = %s"""
                    try:
                        with connect.cursor() as cursor:
                            # Передаем SQL запрос БД
                            cursor.execute(selectDb, (message, location[3:], id_task))
                            # Передать переменной ответ от БД
                            task = cursor.fetchall()
                            # Если элементы, удовлетворяющие запросу поиска существуют
                            if task is not None:
                                # Ищем Id необходимой задачи
                                id_task = task[0]["id"]
                                # Записать новое местоположение
                                next_loc = nextNodeProject("330", str(id_task))
                            else:
                                print("Error in nextNodeSearch351")
                                # При ошибке указать старое местоположение
                                next_loc = location
                    except:
                        print("Error in nextNode351")
                        # При ошибке указать старое местоположение
                        next_loc = location
                else:
                    print("Error in nextNode351")
                    # При ошибке указать старое местоположение
                    next_loc = location
            else:
                print("Error in nextNode351")
                # При ошибке указать старое местоположение
                next_loc = location
        if loc_type == "330":
            # Действия, при сообщении
            if message.lower() == "назад":
                # Записать новое местоположение
                next_loc = nextNodeProject("351", str(get_main_list(location[3:], connect)))
            elif message.lower() == "меню":
                # Записать новое местоположение
                next_loc = mainMenu()
            elif message.lower() == "удалить":
                next_loc = nextNodeProject("351", str(get_main_list(location[3:], connect)))
                delete_task(location[3:], connect)
            elif message.lower() == "переименовать":
                next_loc = nextNodeProject("331", location[3:])
            elif message.lower() == "изменить описание":
                next_loc = nextNodeProject("332", location[3:])
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если узел относится к изменению задачи
        if loc_type == "331":
            # Реакция на сообщения
            if message.lower() == "назад":
                next_loc = goBack(vk_id, connect, location)
            elif message.lower() == "меню":
                next_loc = mainMenu()
            else:
                # Обновить имя задачи
                update_name_task(location[3:], message, connect)
                # Присвоить новое местоположение
                next_loc = nextNodeProject("330", location[3:])
        # Если узел относится к изменению задачи
        if loc_type == "332":
            # Реакция на сообщения
            if message.lower() == "назад":
                next_loc = nextNodeProject("330", location[3:])
            elif message.lower() == "меню":
                next_loc = mainMenu()
            else:
                # Обновить описание задачи
                update_description_task(location[3:], message, connect)
                # Присвоить новое местоположение
                next_loc = nextNodeProject("330", location[3:])
        # Если тип относится к созданию задач
        if loc_type == "301":
            # Реакция на сообщения
            if message.lower() == "да":
                next_loc = nextNodeProject("332", location[3:])
            elif message.lower() == "нет":
                next_loc = nextNodeProject("350", str(get_main_list(location[3:], connect)))
            else:
                # Присвоить новое местоположение
                next_loc = location
    # Если тип относится к техническим, неизменным элементам
    if loc_type[:1] == "5":
        # Если тип обозначает главное меню
        if loc_type == "500":
            # Действия, при сообщении
            if message.lower() == "проекты":
                # Записать новое местоположение
                next_loc = nextNodeProject("150", location[3:])
            elif message.lower() == "простые списки":
                # Записать новое местоположение
                next_loc = nextNodeProject("250", str(get_null_project(vk_id, connect)))
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
        # Если тип узла означает начало работы с проектом
        if loc_type == "501":
            # Действия, при сообщении
            if message.lower() == "начать":
                # Записать новое местоположение
                next_loc = nextNodeProject("500")
            else:
                print("Error in nextNode")
                # При ошибке указать старое местоположение
                next_loc = location
    return next_loc


def nextNodeProject(type_obj, id_project="0"):
    """
    Функция для сборки нового типа и ID проекта
    :param type_obj: Новый тип проекта
    :param id_project: Новый ID проекта
    :return: Возвращает новое местоположение пользователя
    """
    # Собирать местоположение из типа и ID
    loc = type_obj + id_project
    return loc


def goBack(vk_id, connect, location):
    """
    Функция для возвращения пользователя на прошлую локацию
    :param vk_id: ID пользователя ВК
    :param connect: Соединение с базой данных
    :param location: Текущая локация
    :return: Новое местоположение пользователя
    """
    # Сообщение для получения необходимого юзера
    selectDb = """select * from test_table_users where vk_id = %s"""
    # Попытка получить старое местоположение пользователя
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDb, str(vk_id))
            old_loc = cursor.fetchall()
            # Получение старого местоположения через ключ
            old_loc = old_loc[0]["old_location"]
    # При неудаче пищем об ошибке и оставляем пользователя на нынешнем местоположении
    except:
        print("Error in goBack")
        old_loc = location
    # Возвращаем старое местоположение
    return old_loc


def mainMenu():
    """
    Функция для возвращения пользователя в главное меню
    :return: Местоположение главного меню
    """
    return "5000"


def newMess(location, connect, vk_id=None, message=None):
    """
    Функция для создания сообщения для пользователя
    :param location: Новое месторасположение пользователя
    :param connect: Соединение с БД
    :param vk_id: ID пользователя ВК
    :param message: Сообщение пользователя
    :return: список содержащий текст и клавиатуру ВК
    """
    # Переменная для хранения текста
    text = ""
    # Имя проекта
    name_proj = ""
    # Переменная для хранения типа местоположения
    loc_type = location[:3]
    # Если тип относится к проектам
    if loc_type[:1] == "1":
        # Если тип проекта относится к дополнительным задачам
        if loc_type == "150":
            # Подготовить SQL запрос на вывод значений для БД
            selectDb = """select * from test_table_projects where vk_user = %s"""
            try:
                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(vk_id))
                    # Передать переменной все проекты пользователя
                    loc = cursor.fetchall()
                    # Удалить из списка нулевой проект
                    loc = delete_null_project(loc)
                    if len(loc) > 0:
                        # Передать в переменную text все названия проктов пользователя
                        text = "Проекты:\n"
                        for i in range(len(loc)):
                            text = text + loc[i]["project_name"]
                            text = text + " (" + str(loc[i]["id"]) + ")"
                            text = text + "\n"
                    else:
                        text = "У вас нет доступных  проектов"
                    # Сбор клавиатуры ВК
                    startKeyboard = {
                        "one_time": True,
                        "buttons": [
                            [getButton("Выбрать", color="primary")],
                            [getButton("Создать", color="primary")],
                            [getButton("Удалить", color="primary")],
                            [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                        ]
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error150")
        # Если тип проекта относится к дополнительным задачам
        if loc_type == "151":
            # Подготовить SQL запрос на вывод значений для БД
            selectDb = """select * from test_table_projects where vk_user = %s"""
            try:
                text = "Выберите проект"

                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(vk_id))
                    # Передать переменной все проекты пользователя
                    loc = cursor.fetchall()
                    # Удалить нулевой проект
                    loc = delete_null_project(loc)
                    message = changeTrees(message)
                    # Если у пользователя есть не больше 4 проектов
                    if len(loc) == 0:
                        text = "На данный момент у вас нет проектов"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": keyboard_page_project(loc, message)
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error151")
        # Если узел для работы с проектами относится к изменяющему
        if loc_type == "130":
            text = "Проект: "
            # Подготовить SQL запросы на вывод значений для БД
            selectDbProject = """select * from test_table_projects where vk_user = %s and id = %s"""
            try:
                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDbProject, (str(vk_id), str(location[3:])))
                    # Передать переменной необходимый проект
                    loc = cursor.fetchall()
                    # Передать тексту имя проекта
                    text = text + loc[0]["project_name"]

                    # Собираем клавиатуру пользователя
                    startKeyboard = {
                        "one_time": True,
                        "buttons": [
                            [getButton("Перейти к спискам", color="primary")],
                            [getButton("Переименовать", color="primary")],
                            [getButton("Удалить", color="primary")],
                            [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                        ]
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error130")
        # Ести тип узла относится к созданию проектов
        if loc_type == "100":
            # Зададать, отображаемый для пользователя, текст
            text = "Назовите проект: "
            if get_old_mess(vk_id, connect) == "aylb1gq7":
                text = "Извините, но вы не можете так назвать проект\nВыберите другое название"
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error100")
        # Если тип узла относится к удалению проектов
        if loc_type == "120":
            # Подготовить SQL запрос на вывод проект для БД
            selectDb = """select * from test_table_projects where vk_user = %s"""
            # хранилице кнопок
            buttons = []
            try:
                # Задать, видимый пользователем, текст
                text = "Выберите какой проект стоит удалить"

                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(vk_id))
                    # Передать переменной все проекты пользователя
                    loc = cursor.fetchall()
                    # Удалить из списка нулевой проект
                    loc = delete_null_project(loc)
                    message = changeTrees(message)
                    # Если у пользователя есть проекты
                    if len(loc) == 0:
                        text = "Вам нечего удалять"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": keyboard_page_project(loc, message)
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error120")
        # Если узел для работы с проектами относится к изменению проекта
        if loc_type == "131":
            # Зададать, отображаемый для пользователя, текст
            text = "Дайте новое имя проекту: "
            if get_old_mess(vk_id, connect) == "aylb1gq7":
                text = "Извините, но вы не можете так назвать проект\nВыберите другое название"
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Назад", color="primary")],
                        [getButton("Меню", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error131")
    # Если тип узлов относится ко спискм
    if loc_type[:1] == "2":
        # Если тип узла относится к техническим элементам спискаов
        if loc_type == "250":
            name_proj = get_name_project(location[3:], connect)
            if name_proj != "aylb1gq7":
                text = name_proj + "\\"
            else:
                text = "\\"
            # Создать SQL запрос на списки, принадлжещие одному проекту
            selectDb = """select * from test_table_lists where id_project = %s"""
            try:
                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(location[3:]))
                    # Передать переменной все списки пользователя
                    lists = cursor.fetchall()
                    # Если у проекта есть списки
                    if len(lists) > 0:
                        text = text + "Списки:\n"
                        # Для всех списков
                        for i in range(len(lists)):
                            # Добавляем названия списков в сообщение для пользователя
                            text = text + lists[i]["list_name"]
                            text = text + "\n"
                    else:
                        text = "\nУ вас нет доступных списков в этом проекте"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": [
                            [getButton("Выбрать", color="primary")],
                            [getButton("Создать", color="primary")],
                            [getButton("Удалить", color="primary")],
                            [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                        ]
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error250")
        # Ести тип узла относится к созданию списков
        if loc_type == "200":
            # Зададать, отображаемый для пользователя, текст
            text = "Назовите список: "
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error200")
        # Если тип узла относится к удалению списков
        if loc_type == "220":
            # Подготовить SQL запрос на вывод списков для БД
            selectDb = """select * from test_table_lists where id_project = %s"""
            # хранилице кнопок
            buttons = []
            try:
                # Задать, видимый пользователем, текст
                text = "Выберите какой список стоит удалить"

                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(location[3:]))
                    # Передать переменной все списки пользователя
                    loc = cursor.fetchall()
                    message = changeTrees(message)
                    # Если у пользователя есть списки
                    if len(loc) == 0:
                        text = "Вам нечего удалять"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": keyboard_page_list(loc, message)
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error220")
        # Если тип узла для работы со списками относится к дополнительным задачам
        if loc_type == "251":
            # Подготовить SQL запрос на вывод значений для БД
            selectDb = """select * from test_table_lists where id_project = %s"""
            try:
                text = "Выберите список"

                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(location[3:]))
                    # Передать переменной все списки проекта
                    loc = cursor.fetchall()
                    message = changeTrees(message)
                    # Если у пользователя есть не больше 4 списков
                    if len(loc) == 0:
                        text = "На данный момент у вас нет списков"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": keyboard_page_list(loc, message)
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error251")
        # Если узел для работы со списками относится к изменяющему
        if loc_type == "230":
            name_proj = get_name_project(str(get_main_project(location[3:], connect)), connect)
            if name_proj != "aylb1gq7":
                text = name_proj + "\\"
            else:
                text = "\\"
            text = text + "Список:\n"
            # Подготовить SQL запросы на вывод значений для БД
            selectDbList = """select * from test_table_lists where id = %s"""

            try:
                with connect.cursor() as cursor:
                    if checkName(message) is True:
                        # Отправить запрос к БД
                        cursor.execute(selectDbList, str(location[3:]))
                    else:
                        cursor.execute(selectDbList, str(location[3:]))
                    # Передать переменной необходимый список
                    loc = cursor.fetchall()
                    # Передать тексту имя списка
                    text = text + loc[0]["list_name"]

                    # Собираем клавиатуру пользователя
                    startKeyboard = {
                        "one_time": True,
                        "buttons": [
                            [getButton("Перейти к задачам", color="primary")],
                            [getButton("Переименовать", color="primary")],
                            [getButton("Удалить", color="primary")],
                            [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                        ]
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error230")
        # Если узел для работы со списками относится к изменению проекта
        if loc_type == "231":
            # Зададать, отображаемый для пользователя, текст
            text = "Дайте новое имя списку: "
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Назад", color="primary")],
                        [getButton("Меню", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error231")
    # Если тип относится к задачам
    if loc_type[:1] == "3":
        # Если тип узла относится к техническим элементам задач
        if loc_type == "350":
            name_proj = get_name_project(str(get_main_project(location[3:], connect)), connect)
            if name_proj != "aylb1gq7":
                text = name_proj + "\\" + get_name_list(location[3:], connect) + "\\"
            else:
                text = "\\" + get_name_list(location[3:], connect) + "\\"
            # Создать SQL запрос на задачи, принадлжещие одному списку
            selectDb = """select * from test_table_tasks where id_list = %s"""
            try:
                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(location[3:]))
                    # Передать переменной все задачи списка
                    tasks = cursor.fetchall()
                    # Если у списка есть задачи
                    if len(tasks) > 0:
                        text = text + "Задачи:" + "\n"
                        # Для всех списков
                        for i in range(len(tasks)):
                            # Добавляем названия списков в сообщение для пользователя
                            text = text + tasks[i]["task_name"]
                            text = text + "\n"
                    else:
                        text = text + "\nУ вас нет доступных задач в этом списке"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": [
                            [getButton("Выбрать", color="primary")],
                            [getButton("Создать", color="primary")],
                            [getButton("Удалить", color="primary")],
                            [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                        ]
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error350")
        # Ести тип узла относится к созданию задач
        if loc_type == "300":
            # Зададать, отображаемый для пользователя, текст
            text = "Назовите задачу: "
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error300")
        # Если тип узла относится к удалению задач
        if loc_type == "320":
            # Подготовить SQL запрос на вывод задач для БД
            selectDb = """select * from test_table_tasks where id_list = %s"""
            # хранилице кнопок
            buttons = []
            try:
                # Задать, видимый пользователем, текст
                text = "Выберите какую задачу стоит удалить"

                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(location[3:]))
                    # Передать переменной все задачи пользователя
                    loc = cursor.fetchall()
                    message = changeTrees(message)
                    # Если у пользователя есть задачи
                    if len(loc) == 0:
                        text = "Вам нечего удалять"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": keyboard_page_task(loc, message)
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error320")
        # Если тип узла для работы с задачами относится к дополнительным задачам
        if loc_type == "351":
            # Подготовить SQL запрос на вывод значений для БД
            selectDb = """select * from test_table_tasks where id_list = %s"""
            try:
                text = "Выберите задачу"

                with connect.cursor() as cursor:
                    # Отправить запрос к БД
                    cursor.execute(selectDb, str(location[3:]))
                    # Передать переменной все задачи списка
                    loc = cursor.fetchall()
                    message = changeTrees(message)
                    # Если у пользователя есть не больше 4 задач
                    if len(loc) == 0:
                        text = "На данный момент у вас нет задач"
                    startKeyboard = {
                        "one_time": True,
                        "buttons": keyboard_page_task(loc, message)
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error351")
        # Если узел для работы с задачами относится к изменяющему
        if loc_type == "330":
            name_proj = get_name_project(str(get_main_project(get_main_list(location[3:], connect), connect)), connect)
            if name_proj != "aylb1gq7":
                text = name_proj + get_name_list(get_main_list(location[3:], connect), connect) + "\\"
            else:
                text = "\\" + get_name_list(get_main_list(location[3:], connect), connect) + "\\"
            text = "Задача: \n"
            # Подготовить SQL запросы на вывод значений для БД
            selectDbTask = """select * from test_table_tasks where id = %s"""

            try:
                with connect.cursor() as cursor:
                    if checkName(message) is True:
                        # Отправить запрос к БД
                        cursor.execute(selectDbTask, str(location[3:]))
                    else:
                        cursor.execute(selectDbTask, str(location[3:]))
                    # Передать переменной необходимый список
                    loc = cursor.fetchall()
                    # Передать тексту имя задачи
                    text = text + loc[0]["task_name"]
                    if loc[0]["task_description"] is not None:
                        text = text + "\nОписание задачи:\n" + loc[0]["task_description"]
                    # Собираем клавиатуру пользователя
                    startKeyboard = {
                        "one_time": True,
                        "buttons": [
                            [getButton("Переименовать", color="primary")],
                            [getButton("Изменить описание", color="primary")],
                            [getButton("Удалить", color="primary")],
                            [getButton("Назад", color="primary"), getButton("Меню", color="primary")]
                        ]
                    }

                    startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                    startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error330")
        # Если узел для работы с задачами относится к изменению
        if loc_type == "331":
            # Зададать, отображаемый для пользователя, текст
            text = "Дайте новое имя задаче: "
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Назад", color="primary")],
                        [getButton("Меню", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error331")
        # Если узел для работы с задачами относится к изменению
        if loc_type == "332":
            # Зададать, отображаемый для пользователя, текст
            text = "Дайте новое описание задаче: "
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Назад", color="primary")],
                        [getButton("Меню", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error332")
        # Ести тип узла относится к созданию задач
        if loc_type == "301":
            # Зададать, отображаемый для пользователя, текст
            text = "Вы хотите дать описание задачи"
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Да", color="primary"), getButton("Нет", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error301")
    # Если тип относится к техническим, неизменяемым элементам
    if loc_type[:1] == "5":
        # Если тип относится к главному меню
        if loc_type == "500":
            text = "Меню"
            try:
                startKeyboard = {
                    "one_time": True,
                    "buttons": [
                        [getButton("Проекты", color="primary")],
                        [getButton("Простые списки", color="primary")]
                    ]
                }

                startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
                startKeyboard = str(startKeyboard.decode("utf-8"))
            except:
                print("Error500")
        # Если тип узла относится к началу работы с программой
        if loc_type == "501":
            text = "Для началы работы с нашим чат-ботом нажмите \"Начать\""
            startKeyboard = {
                "one_time": True,
                "buttons": [
                    [getButton("Начать", color="primary")]
                ]
            }

            startKeyboard = json.dumps(startKeyboard, ensure_ascii=False).encode("utf-8")
            startKeyboard = str(startKeyboard.decode("utf-8"))
    return [text, startKeyboard]


def setLastID(connect):
    """
    Функция для задания актуального ID для элементов БД
    :param connect: соединение с БД
    :return: Возвращает ID элемента
    """
    selectDBLastID = """select * from test_last_id"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDBLastID)
            freeID = cursor.fetchall()
            update_last_ID(freeID[0]["last_id"] + 1, connect)
            return freeID[0]["last_id"]
    except:
        print("Ошибка в изменении последнего ID")
        return 0


def delete_null_project(list_projects):
    """
    Удаляет из полученного списка проектов нулевой список
    :param list_projects: Список проекстов
    :return: Возвращает отредактированный список
    """
    # Для всех элементов списка
    for proj in range(len(list_projects)):
        # Если имя проекта это имя нулевого списка
        if list_projects[proj]["project_name"] == "aylb1gq7":
            # Удалить  нулевой проект
            list_projects.pop(proj)
            break
    return list_projects

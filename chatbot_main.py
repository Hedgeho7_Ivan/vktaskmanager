from addFunctions import *
from deleteFunctions import *
from updateFunctions import *
from service_functions.get_functions import *
from service_functions.keyboard import *
from service_functions.check_functions import *
from chatbot_main import *
from bunch import *
from chatbot_structure import *


import vk_api
from ImportantData import vk_token
from vk_api.longpoll import VkLongPoll, VkEventType


# Объявление vk
vk = vk_api.VkApi(token=vk_token())
vk._auth_token()
vk.get_api()

# Объявляем longpool
longpoll = VkLongPoll(vk)

# Подключаемся к БД
connect = connectDB()


# Условие продолжения работы
while True:
    for event in longpoll.listen():
        if event.to_me:
            if event.type == VkEventType.MESSAGE_NEW:
                # Передать переменной сообщение пользователя в нижнем регистре
                message = event.text
                # Передать переменной id собеседника
                vk_id = event.peer_id
                # Подготовить SQL запрос для получения информации о пользователе
                messageDbBot = """select * from test_table_users where vk_id = %s;"""
                try:
                    with connect.cursor() as cursor:
                        # Передать SQL запрос БД
                        cursor.execute(messageDbBot, str(vk_id))
                        # Передать информацию о пользователе переменной
                        loc = cursor.fetchall()
                        if len(loc) == 0:
                            add_user(vk_id, "5010", "5010", "0", connect=connect)
                            add_project(str(setLastID(connect)), "aylb1gq7", str(vk_id), connect)
                            new_loc = "5010"
                            loc = new_loc
                        else:
                            # Получить местоположение пользователя
                            loc = loc[0]["location"]
                            # Вычислить новое местоположение пользователя и среагировать на сообщение
                            new_loc = nextNode(loc, message, connect, vk_id)
                        # Если пользователь хочет вернуться на прошлое местоположение
                        if message.lower() == "назад":
                            # Подготовить SQL запрос на получение старого местоположения пользователя
                            old_messageBD = """select * from test_table_users where vk_id = %s"""
                            try:
                                with connect.cursor() as cursor:
                                    # Отправить запрос БД
                                    cursor.execute(old_messageBD, str(vk_id))
                                    # Передать старое местоположение пользователя переменной нынешнего местоположения
                                    message = cursor.fetchall()[0]["old_message"].lower()
                            except:
                                vk.method("messages.send",
                                          {"peer_id": event.peer_id, "message": "Ошибка в старом сообщении",
                                           "random_id": 0})
                        # Вычислить сообщения для оправки пользователю
                        new_mess = newMess(new_loc, connect, vk_id, message)
                        # Обновить местоположение пользователя
                        update_user_location(new_loc, vk_id, connect)
                        # Если значения старого местоположения и нового не совпадают
                        if loc != new_loc:
                            # Обновить старое местоположение
                            update_user_old_location(vk_id, loc, connect)
                        # Обновить старое сообщение
                        update_user_old_message(vk_id, message, connect)
                        # Отправить пользователю сообщение
                        vk.method("messages.send",
                                  {"peer_id": event.peer_id, "message": new_mess[0],
                                   "random_id": 0, "keyboard": new_mess[1]})
                # В случае неудачи, оповещаем о ошибке
                except:
                    vk.method("messages.send",
                              {"peer_id": event.peer_id, "message": "Ошибка",
                               "random_id": 0})
connect.close()

from bunch import connectDB


def update_user_location(location, vk_id, connect=None):
    """Обновить местоположение пользователя в БД

    Аргументы:
    location -- нынешнее местоположение пользователя
    vk_id -- идентификатор пользователя в вк
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_users set location = %s where vk_id = %s;"""
    # Соединяемся с базой данных
    #connect = connectDB()
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (location, str(vk_id)))
            cursor.execute("""select * from test_table_users;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
    # Отсоединяемся от базы данных
    #connect.close()


def update_user_time(time, vk_id, connect):
    """Обновить часовой пояс пользователя

        Аргументы:
        time -- часовой пояс пользователя
        vk_id -- идентификатор пользователя в вк
        """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_users set time_zone = %s where vk_id = %s;"""
    # Соединяемся с базой данных
    #connect = connectDB()
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (time, vk_id))
            cursor.execute("""select * from test_table_users;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
    # Отсоединяемся от базы данных
    #connect.close()


def update_name_project(obj_id, name, connect):
    """Изменить имя проекта

    Аргументы:
    obj_id -- идентификатор проекта
    name -- имя проекта
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_projects set project_name = %s where id = %s;"""
    # Соединяемся с базой данных
    #connect = connectDB()
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (name, obj_id))
            cursor.execute("""select * from test_table_projects;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
    # Отсоединяемся от базы данных
    #connect.close()


def update_dlc_users_project(obj_id, dlc_users, connect):
    """Изменить участников проекта

    Аргументы:
    obj_id -- идентификатор проекта
    dlc_ -- имя проекта
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_projects set dlc_users = %s where id = %s;"""
    # Соединяемся с базой данных
    #connect = connectDB()
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (dlc_users, obj_id))
            cursor.execute("""select * from test_table_projects;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
    # Отсоединяемся от базы данных
    #connect.close()


def update_name_list(obj_id, name, connect):
    """Изменить названия списка задач

    Аргументы:
    obj_id -- индентификатор списка
    name -- название списка
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_lists set list_name = %s where id = %s;"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (name, obj_id))
            cursor.execute("""select * from test_table_lists;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка в изменении имени списка")


def update_list_workers(obj_id, workers, connect):
    """Изменить ответственных за список задач

    Аргументы:
    obj_id -- идентификатор списка
    workers -- отвественные за список
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_lists set workers = %s where id = %s;"""
    # Соединяемся с базой данных
    #connect = connectDB()
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (workers, obj_id))
            cursor.execute("""select * from test_table_lists;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
    # Отсоединяемся от базы данных
    #connect.close()


def update_name_task(obj_id, name, connect):
    """
    Изменить название задачи
    Аргументы:
    obj_id -- идентификатор задачи
    name -- название задачи
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_tasks set task_name = %s where id = %s;"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (name, obj_id))
            cursor.execute("""select * from test_table_tasks;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")


def update_description_task(obj_id, description, connect):
    """
    Изменить описание задачи
    Аргументы:
    obj_id -- идентификатор задачи
    description -- описание задачи
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_tasks set task_description = %s where id = %s;"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (description, str(obj_id)))
            cursor.execute("""select * from test_table_tasks;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")


def update_task_workers(obj_id, workers, connect):
    """Изменить ответственных за задачу

    Аргументы:
    obj_id -- идентификатор задачи
    workers -- ответственные за задачу
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_tasks set workers = %s where id = %s;"""
    # Соединяемся с базой данных
    #connect = connectDB()
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (workers, obj_id))
            cursor.execute("""select * from test_table_tasks;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
    # Отсоединяемся от базы данных
    #connect.close()


def update_name_tags(obj_id, tags, connect):
    """Изменить метки задачи

    Аргументы:
    obj_id -- идентификатор задачи
    tags -- метки задачи
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_tasks set tags = %s where id = %s;"""
    # Соединяемся с базой данных
    #connect = connectDB()
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (tags, obj_id))
            cursor.execute("""select * from test_table_tasks;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
    # Отсоединяемся от базы данных
    #connect.close()


def update_user_old_location(vk_id, location, connect):
    """
    Функция для изменения старого местоположения у пользователя
    :param vk_id: ID пользователя ВК
    :param location: Старое местоположение пользователя
    :param connect: Соединение с БД
    :return:
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_users set old_location = %s where vk_id = %s"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (location, str(vk_id)))
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка в обновлении старого местоположения")


def update_user_old_message(vk_id, message, connect):
    """
    Функция для сохранения старого сообщения пользователя
    :param vk_id: ID пользователя ВК
    :param message: Сообщение пользователя
    :param connect: Соединение с БД
    :return:
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbUpdate = """update test_table_users set old_message = %s where vk_id = %s"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbUpdate, (message, str(vk_id)))
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка в обновлении старого сообщения")


def update_last_ID(new_free_ID, connect):
    updateID = """update test_last_id set last_id = %s where last_id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(updateID, (str(new_free_ID), str(new_free_ID - 1)))
            connect.commit()
    except:
        print("Ошибка в обновлении старого ID")

import math


def checkName(message):
    """
    Функция для проверки соответствия формата названий элементов для клавиатуры
    :param message: сообщение, отправленное пользователем
    :return: True или False
    """
    # Если длина сообщения меньше или равна 5 символам
    if len(message) <= 5:
        return False
    # Если сообщение не заканчивается )
    elif message[-1] != ")":
        return False
    # Если в сообщении нет (
    elif message.find("(") == -1:
        return False
    # Если между скоками не число
    elif message[message.find("(") + 1:-1].isdigit() is False:
        return False
    # Если перед открывающей скобкой больше 1 элемента
    elif 0 <= message.find("(") <= 1:
        return False
    # Если элемент перед открывающей скобкой не пробел
    elif message[message.find("(") - 1] != " ":
        return False
    else:
        return True


def checkElementProject(message, vk_id, connect):
    """
    Функция для обнаружения элемента среди проектов для сообщений формата: название (id)
    :param message: сообщение пользователя
    :param vk_id: ID пользователя в ВК
    :param connect: сокдинение с БД
    :return: True или False
    """
    # Создать SQL запрос на существование элемента в БД
    selectDB = """select * from test_table_projects where vk_user = %s and project_name = %s and id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, (vk_id, messageCut(message)[0], messageCut(message)[1]))
            project_instans = cursor.fetchall()
            # Если элемента в БД нет
            if len(project_instans) == 0:
                return False
            else:
                return True
    except:
        print("Проблемы с обнаружением элемента")


def checkMoveButton(message, vk_id, connect):
    """
    Функция, определяющая подходит ли сообщение под клавиатуру от страниц
    :param message: Сообщение пользователя
    :param vk_id: ID пользователя в ВК
    :param connect: соединение с БД
    :return: True или False
    """
    # Подготовить SQL запрос на получение информации о пользователе для БД
    selectDB = """select * from test_table_users where vk_id = %s"""
    # Изменить сообщение, заменяя ">>"("<<"), занимающих 1 место на аналогичные 2 элемента
    message = changeTrees(message)
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, str(vk_id))
            # Передадим переменной значение старого местоположения
            old_button = changeTrees(cursor.fetchall()[0]["old_message"])
            # Если длина сообщения пользователя меньше 3
            if len(message) < 3:
                return False
            # Если сообщение имеет в начале <<, а в конце число
            elif message[:2] == "<<" and message[2:].isdigit() is True:
                # Если длина старого сообщения не меньше 3
                if len(old_button) >= 3:
                    # Если старое сообщение имеет в начале <<, а в конце число
                    if old_button[:2] == "<<" or old_button[2:].isdigit() is True:
                        # Если число в нынешнем сообщение на 1 меньше, чем в старом сообщении
                        if int(message[2:]) == int(old_button[2:]) - 1:
                            return True
                        else:
                            return False
                    # Если старое сообщение имеет в начале >>, а в конце число
                    elif old_button[-2:] == ">>" or old_button[:-2].isdigit() is True:
                        # Если число в нынешнем сообщение на 1 меньше, чем в старом сообщении
                        if int(message[2:]) == int(old_button[:-2]) - 1:
                            return True
                        else:
                            return False
                    else:
                        return False
                else:
                    return False
            # Если сообщение имеет в начале >>, а в конце число
            elif message[-2:] == ">>" and message[:-2].isdigit() is True:
                # Если в сообщении число равно 1 и старое сообщение либо равно <<0 либо длина старого сообщения меньше 3
                if message[:-2] == "1" and (old_button == "<<0" or len(old_button) < 3):
                    return True
                # Если в сообщении число равно 1 и длина старого сообщения не меньше 3
                elif message[:-2] == "1" and len(old_button) >= 3:
                    # Если существует аналогичное сообщение 1>>
                    if (old_button[:2] == "<<" or old_button[2:].isdigit() is True) or \
                            (old_button[-2:] == ">>" or old_button[:-2].isdigit() is True):
                        return False
                    else:
                        return True
                # Если длина старого сообщения не меньше 3
                elif len(old_button) >= 3:
                    # Если старое сообщение имеет в начале <<, а в конце число
                    if old_button[:2] == "<<" or old_button[2:].isdigit() is True:
                        # Если число в новом сообщение на 1 меньше чем в старом
                        if int(message[:-2]) == int(old_button[2:]) + 1:
                            return True
                        else:
                            return False
                    # Если старое сообщение имеет в начале >>, а в конце число
                    elif old_button[-2:] == ">>" or old_button[:-2].isdigit() is True:
                        # Если число в новом сообщение на 1 меньше чем в старом
                        if int(message[:-2]) == int(old_button[:-2]) + 1:
                            return True
                        else:
                            return False
                    else:
                        return False
    except:
        print("Проблема в проверки кнопки на перемещение по списку")
        return False


def checkMoveKeybord(mess, list_of_elements):
    """
    Проверка того, что сообщение явлется реакцией на нажитие клавиатуры
    :param mess: Сообщение
    :param list_of_elements: Список элементов
    :return: True или False
    """
    # Переменной присвоить значение возможного кол-ва страниц
    page = math.ceil(len(list_of_elements) / 4)
    # Если длина списка меньшу 3
    if len(mess) < 3:
        return False
    # Если сообщение имеет в начале <<, а в конце число
    elif mess[:2] == "<<" or mess[2:].isdigit() is True:
        # Если число сообщения меньше 0 или больше кол-ва страниц
        if int(mess[2:]) < 0 or int(mess[2:]) >= page:
            return False
        else:
            return True
    # Если сообщение имеет в начале >>, а в конце число
    elif mess[-2:] == ">>" or mess[2:].isdigit() is True:
        # Если число сообщения меньше 0 или больше кол-ва страниц + 1
        if int(mess[:-2]) < 0 or int(mess[:-2]) >= page + 1:
            return False
        else:
            return True
    else:
        return False


def checkElementList(message, id_list, connect):
    """
    Функция для обнаружения элемента среди списков для сообщений формата: название (id)
    :param message: сообщение пользователя
    :param id_list: ID списка
    :param connect: сокдинение с БД
    :return: True или False
    """
    # Создать SQL запрос на существование элемента в БД
    selectDB = """select * from test_table_lists where id_project = %s and list_name = %s and id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, (id_list, messageCut(message)[0], messageCut(message)[1]))
            list_instans = cursor.fetchall()
            # Если элемента в БД нет
            if len(list_instans) == 0:
                return False
            else:
                return True
    except:
        print("Проблемы с обнаружением элемента")


def checkElementTask(message, id_task, connect):
    """
    Функция для обнаружения элемента среди задач для сообщений формата: название (id)
    :param message: сообщение пользователя
    :param id_task: ID задачи
    :param connect: сокдинение с БД
    :return: True или False
    """
    # Создать SQL запрос на существование элемента в БД
    selectDB = """select * from test_table_tasks where id_list = %s and task_name = %s and id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, (id_task, messageCut(message)[0], messageCut(message)[1]))
            task_instans = cursor.fetchall()
            # Если элемента в БД нет
            if len(task_instans) == 0:
                return False
            else:
                return True
    except:
        print("Проблемы с обнаружением элемента")


def messageCut(message):
    """
    Функция для разбития сообщения для элементов
    :param message: Сообщение, отправленное пользователем
    :return: Список, содержащий название и ID элемента
    """
    strCut = []
    i = -1
    # Пока в сообщении не будет найдена открывающая  скобка
    while message[i] != "(":
        # Элемент i будет уменьшаться на единицу
        i = i - 1
    # В список добавить название элемента
    strCut.append(message[:i - 1])
    # В список добавить ID элемента
    strCut.append(message[i + 1:-1])
    return strCut


def changeTrees(message):
    """
    Заменяет в сообщении символ >>(<<) на аналогичную подстроку
    :param message: Сообщение
    :return: Возвращает измененную подстроку
    """
    # Если в сообщении есть знак >>
    if message.find("»") != -1:
        # Передать переменной индекс символа
        a = message.find("»")
        # Передать переменной подстроку сообщения после знака
        message_copy = message[a+1:]
        # Сообрать сообщение заново
        message = message[:a] + ">>" + message_copy
    # Если в сообщении есть знак <<
    elif message.find("«") != -1:
        # Передать переменной индекс символа
        a = message.find("«")
        # Передать переменной подстроку сообщения после знака
        message_copy = message[a + 1:]
        # Сообрать сообщение заново
        message = message[:a] + "<<" + message_copy
    return message

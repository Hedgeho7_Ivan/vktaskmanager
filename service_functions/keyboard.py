from service_functions.get_functions import getButton
from service_functions.check_functions import *


def keyboard_page_project(loc, message):
    """
    Создает клавиатуру для страниц проектов
    :param loc: Проекты юзера
    :param message: Измененное сообщение пользователя
    :return:
    """
    buttons = []
    # Если у пользователя есть не больше 4 проектов
    if 0 < len(loc) <= 4:
        # Собираем динамическую клавиатуру
        for i in range(len(loc)):
            # Элемент не последний
            if i != len(loc) - 1:
                buttons.append([getButton((loc[i]["project_name"] + " (" + str(loc[i]["id"]) + ")"),
                                          color="primary")])
            else:
                buttons.append([getButton((loc[i]["project_name"] + " (" + str(loc[i]["id"]) + ")"),
                                          color="primary")])
                buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
    # Если у пользователя больше 4 проектов
    elif len(loc) > 4:
        try:
            """ Если сообщение не является реакцией на нажатие клавиатуры 
            или был переход к 1-ой странице"""
            if checkMoveKeybord(message, loc) is False or message == "<<0":
                for i in range(4):
                    # Если элемент доступных проектов на данной странице не последний
                    if i != 3:
                        buttons.append([getButton((loc[i]["project_name"] + " (" +
                                                   str(loc[i]["id"]) + ")"), color="primary")])
                    else:
                        buttons.append([getButton((loc[i]["project_name"] + " (" +
                                                   str(loc[i]["id"]) + ")"), color="primary")])
                        buttons.append([getButton("1>>", color="primary")])
                        buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
            # Если сообщение не является реакцией на нажатие клавиатуры
            # или был переход к 1-ой странице
            elif checkMoveKeybord(message, loc) is True or message != "<<0":
                # Присвоить переменной кол-во страниц с проектами
                page = math.ceil(len(loc) / 4)
                # Номер страницы
                if message[-2:] == ">>":
                    num_button = int(message[:-2])
                else:
                    num_button = int(message[2:])
                # Если номер кнопки равен последней странице
                if num_button == page - 1:
                    # То присвоить оставшееся кол-во кнопок
                    keyboard_range = len(loc) - 4 * num_button
                else:
                    keyboard_range = 4
                # Для всех проектов на определенной странице
                for i in range(keyboard_range):
                    # Если проект на последний
                    if i != keyboard_range - 1:
                        buttons.append([getButton((loc[i + 4 * num_button]["project_name"] +
                                                   " (" + str(loc[i + 4 * num_button]["id"]) +
                                                   ")"), color="primary")])
                    else:
                        buttons.append([getButton((loc[i + 4 * num_button]["project_name"] +
                                                   " (" + str(loc[i + 4 * num_button]["id"]) +
                                                   ")"), color="primary")])
                        # Если номер странице равен кол-ву страниц (-1)
                        if page - 1 == num_button:
                            buttons.append([getButton("<<" + str(num_button - 1), color="primary")])
                            buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
                        else:
                            buttons.append([getButton("<<" + str(num_button - 1), color="primary"),
                                            getButton(str(num_button + 1) + ">>", color="primary")])
                            buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
        except:
            print("Error epic")
    else:
        buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
    return buttons


def keyboard_page_list(loc, message):
    """
    Создает клавиатуру для страниц списков
    :param loc: Списки юзера
    :param message: Измененное сообщение пользователя
    :return:
    """
    buttons = []
    # Если у пользователя есть не больше 4 списков
    if 0 < len(loc) <= 4:
        # Собираем динамическую клавиатуру
        for i in range(len(loc)):
            # Если элемент не последний
            if i != len(loc) - 1:
                buttons.append([getButton((loc[i]["list_name"] + " (" + str(loc[i]["id"]) + ")"),
                                          color="primary")])
            else:
                buttons.append([getButton((loc[i]["list_name"] + " (" + str(loc[i]["id"]) + ")"),
                                          color="primary")])
                buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
    # Если у пользователя больше 4 списков
    elif len(loc) > 4:
        try:
            """ Если сообщение не является реакцией на нажатие клавиатуры 
            или был переход к 1-ой странице"""
            if checkMoveKeybord(message, loc) is False or message == "<<0":
                for i in range(4):
                    # Если элемент доступных списков на данной странице не последний
                    if i != 3:
                        buttons.append([getButton((loc[i]["list_name"] + " (" +
                                                   str(loc[i]["id"]) + ")"), color="primary")])
                    else:
                        buttons.append([getButton((loc[i]["list_name"] + " (" +
                                                   str(loc[i]["id"]) + ")"), color="primary")])
                        buttons.append([getButton("1>>", color="primary")])
                        buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
            # Если сообщение не является реакцией на нажатие клавиатуры
            # или был переход к 1-ой странице
            elif checkMoveKeybord(message, loc) is True or message != "<<0":
                # Присвоить переменной кол-во страниц с списками
                page = math.ceil(len(loc) / 4)
                # Номер страницы
                if message[-2:] == ">>":
                    num_button = int(message[:-2])
                else:
                    num_button = int(message[2:])
                # Если номер кнопки равен последней странице
                if num_button == page - 1:
                    # То присвоить оставшееся кол-во кнопок
                    keyboard_range = len(loc) - 4 * num_button
                else:
                    keyboard_range = 4
                # Для всех списков на определенной странице
                for i in range(keyboard_range):
                    # Если список не последний
                    if i != keyboard_range - 1:
                        buttons.append([getButton((loc[i + 4 * num_button]["list_name"] +
                                                   " (" + str(loc[i + 4 * num_button]["id"]) +
                                                   ")"), color="primary")])
                    else:
                        buttons.append([getButton((loc[i + 4 * num_button]["list_name"] +
                                                   " (" + str(loc[i + 4 * num_button]["id"]) +
                                                   ")"), color="primary")])
                        # Если номер странице равен кол-ву страниц (-1)
                        if page - 1 == num_button:
                            buttons.append([getButton("<<" + str(num_button - 1), color="primary")])
                            buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
                        else:
                            buttons.append([getButton("<<" + str(num_button - 1), color="primary"),
                                            getButton(str(num_button + 1) + ">>", color="primary")])
                            buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
        except:
            print("Error epic")
    else:
        buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
    return buttons


def keyboard_page_task(loc, message):
    """
    Создает клавиатуру для страниц задач
    :param loc: Задачи юзера
    :param message: Измененное сообщение пользователя
    :return:
    """
    buttons = []
    # Если у пользователя есть не больше 4 задач
    if 0 < len(loc) <= 4:
        # Собираем динамическую клавиатуру
        for i in range(len(loc)):
            # Если элемент не последний
            if i != len(loc) - 1:
                buttons.append([getButton((loc[i]["task_name"] + " (" + str(loc[i]["id"]) + ")"),
                                          color="primary")])
            else:
                buttons.append([getButton((loc[i]["task_name"] + " (" + str(loc[i]["id"]) + ")"),
                                          color="primary")])
                buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
    # Если у пользователя больше 4 задач
    elif len(loc) > 4:
        try:
            """ Если сообщение не является реакцией на нажатие клавиатуры 
            или был переход к 1-ой странице"""
            if checkMoveKeybord(message, loc) is False or message == "<<0":
                for i in range(4):
                    # Если элемент доступных задач на данной странице не последний
                    if i != 3:
                        buttons.append([getButton((loc[i]["task_name"] + " (" +
                                                   str(loc[i]["id"]) + ")"), color="primary")])
                    else:
                        buttons.append([getButton((loc[i]["task_name"] + " (" +
                                                   str(loc[i]["id"]) + ")"), color="primary")])
                        buttons.append([getButton("1>>", color="primary")])
                        buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
            # Если сообщение не является реакцией на нажатие клавиатуры
            # или был переход к 1-ой странице
            elif checkMoveKeybord(message, loc) is True or message != "<<0":
                # Присвоить переменной кол-во страниц с задачи
                page = math.ceil(len(loc) / 4)
                # Номер страницы
                if message[-2:] == ">>":
                    num_button = int(message[:-2])
                else:
                    num_button = int(message[2:])
                # Если номер кнопки равен последней странице
                if num_button == page - 1:
                    # То присвоить оставшееся кол-во кнопок
                    keyboard_range = len(loc) - 4 * num_button
                else:
                    keyboard_range = 4
                # Для всех задач на определенной странице
                for i in range(keyboard_range):
                    # Если задача не последняя
                    if i != keyboard_range - 1:
                        buttons.append([getButton((loc[i + 4 * num_button]["list_name"] +
                                                   " (" + str(loc[i + 4 * num_button]["id"]) +
                                                   ")"), color="primary")])
                    else:
                        buttons.append([getButton((loc[i + 4 * num_button]["list_name"] +
                                                   " (" + str(loc[i + 4 * num_button]["id"]) +
                                                   ")"), color="primary")])
                        # Если номер странице равен кол-ву страниц (-1)
                        if page - 1 == num_button:
                            buttons.append([getButton("<<" + str(num_button - 1), color="primary")])
                            buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
                        else:
                            buttons.append([getButton("<<" + str(num_button - 1), color="primary"),
                                            getButton(str(num_button + 1) + ">>", color="primary")])
                            buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
        except:
            print("Error epic")
    else:
        buttons.append([getButton("Назад", color="primary"), getButton("Меню", color="primary")])
    return buttons

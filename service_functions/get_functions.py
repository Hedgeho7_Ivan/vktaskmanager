import json


def getButton(label, color, payload=''):
    """
    Функция для создания кнопок
    :param label: Текст
    :param color: Цвет
    :param payload:
    :return: Возвращает кнопку
    """
    return {
        "action": {
            "type": "text",
            "payload": json.dumps(payload),
            "label": label
        },
        "color": color
    }


def get_old_mess(vk_id, connect):
    """
    Функция для получения старого сообщения пользователя
    :param vk_id: ID пользователя vk
    :param connect: Соединение с БД
    :return: Возвращает старое сообщение пользователя
    """
    # Подготовить SQL запрос на получение информации о пользователе
    selectBD = """select * from test_table_users where vk_id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectBD, str(vk_id))
            return cursor.fetchall()[0]["old_message"]
    except:
        print("Ошибка не получилось получить сообщение")
        return "pass"


def get_null_project(vk_id, connect):
    """
    Функция для получения ID нулевого проекта
    :param vk_id: ID пользователя вк
    :param connect: Соединение с БД
    :return: Возвращает ID либо выдает ошибку
    """
    # Подготовить запрос SQL на получение нулевого проекта
    selectDB = """select * from test_table_projects where vk_user = %s and project_name = %s"""

    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, (str(vk_id), "aylb1gq7"))
            return cursor.fetchall()[0]["id"]
    except:
        print("Ошибка при получении нулевого проекта")


def get_main_project(list_id, connect):
    """
    Функция для получения id проекта к которому принадлежит список
    :param list_id: ID списка
    :param connect: Соединение с БД
    :return: Возвращает ID проекта или ошибку
    """
    selectDB = """select * from test_table_lists where id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, str(list_id))
            idProject = cursor.fetchall()
            return idProject[0]["id_project"]
    except:
        print("Ошибка при получении id проекта к которому принадлежит список")


def get_main_list(task_id, connect):
    """
    Функция для получения id списка к которому принадлежит задача
    :param task_id: ID задачи
    :param connect: Соединение с БД
    :return: Возвращает ID списка или ошибку
    """
    selectDB = """select * from test_table_tasks where id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, str(task_id))
            idList = cursor.fetchall()
            return idList[0]["id_list"]
    except:
        print("Ошибка при получении id проекта к которому принадлежит список")


def get_name_project(id_project, connect):
    """
    Получить имя проекта
    :param id_project: ID проекта
    :param connect: Соединение с БД
    :return: Возвращает имя проекта или ошибку
    """
    # Написать запрос SQL для получения необходимого проекта
    selectDB = """select * from test_table_projects where id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, id_project)
            project = cursor.fetchall()
            # Если проект есть
            if len(project) != 0:
                return project[0]["project_name"]
            else:
                print("Ошибка, проект отсутствует")
    except:
        print("Ошибка подключения к БД")


def get_name_list(id_list, connect):
    """
    Получить имя списка
    :param id_list: ID списка
    :param connect: Соединение с БД
    :return: Возвращает имя списка или ошибку
    """
    # Написать запрос SQL для получения необходимого проекта
    selectDB = """select * from test_table_lists where id = %s"""
    try:
        with connect.cursor() as cursor:
            cursor.execute(selectDB, id_list)
            list = cursor.fetchall()
            # Если проект есть
            if len(list) != 0:
                return list[0]["list_name"]
            else:
                print("Ошибка, список отсутствует")
    except:
        print("Ошибка подключения к БД")

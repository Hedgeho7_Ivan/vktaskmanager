from bunch import *


def add_user(vk_id, location, old_location="0", time="0", old_message=None, connect=None):
    """
    Добавить пользователя в таблицу пользователей (БД)
    :param connect: Соединение с БД
    :param vk_id: идентификатор пользователя в вк
    :param location: текущее местоположение пользователя
    :param old_location: старое местоположение пользователя (default 0)
    :param time: временная зона пользователя (default 0)
    :param old_message: старое сообщение пользователя (default None)
    :return:
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbInject = """insert into test_table_users (vk_id, location, time_zone, old_location, old_message) 
values (%s, %s, %s, %s, %s);"""
    # Пробуем передать запрос на добавление элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbInject, (vk_id, location, time, old_location, old_message))
            cursor.execute("""select * from test_table_users;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка при создании пользователя")


def add_project(obj_id, name, main_user, connect=None, dlc_users=None):
    """ Добавить проект в таблицу проектов (БД)

    Аргументы:
    obj_id -- идентификатор проекта
    name -- имя проекта
    main_user -- владелец проекта (пользователь вк)
    dlc_users -- внешние участники проекта (пользователи вк) (default None)
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbInject = """insert into test_table_projects (id, project_name, vk_user, dlc_users) values (%s, %s, %s, %s)"""
    # Пробуем передать запрос на добавление элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbInject, (obj_id, name, main_user, dlc_users))
            cursor.execute("""select * from test_table_projects;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")


def add_list(obj_id, name, main_project, workers=None, connect=None):
    """
    Добавить список в таблицу списков (БД)
    Аргументы:
    obj_id -- идентификатор списка
    name -- имя списка
    main_project -- проект к которому принадлежит список
    workers -- ответственные за список (пользователи вк) (default None)
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbInject = """insert into test_table_lists (id, list_name, id_project, workers) values (%s, %s, %s, %s)"""
    # Пробуем передать запрос на добавление элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbInject, (obj_id, name, main_project, workers))
            cursor.execute("""select * from test_table_lists;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")


def add_task(obj_id, name, main_list, description=None, workers=None, tags=None, connect=None):
    """
    Добавить задачу в таблице задач (БД)
    :param obj_id: Идентификатор задачи
    :param name: Имя задачи
    :param main_list: Список к которому принадлежит задача
    :param description: Описание задачи (default None)
    :param workers: Ответственные за задачу (пользователи вк) (default None)
    :param tags: Метки задачи (default None)
    :param connect: Соединение с БД
    :return:
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbInject = """insert into test_table_tasks (id, task_name, task_description, id_list, workers, tags) values \
(%s, %s, %s, %s, %s, %s)"""
    # Пробуем передать запрос на добавление элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbInject, (obj_id, name, description, main_list, workers, tags))
            cursor.execute("""select * from test_table_tasks;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка при создании заачи")

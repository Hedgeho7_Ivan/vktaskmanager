import pymysql
from ImportantData import connection_data


def connectDB():
    """ Создать соединение с базой данных"""
    return pymysql.connect(host=connection_data().get('host'),
                           user=connection_data().get('user'),
                           password=connection_data().get('pass'),
                           db=connection_data().get('db'),
                           charset=connection_data().get('char'),
                           cursorclass=pymysql.cursors.DictCursor
                           )

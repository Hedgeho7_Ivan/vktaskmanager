from bunch import connectDB


def delete_project(obj_id, connect=None):
    """
    Удалить проект
    Аргументы:
    obj_id -- идентификатор проекта
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbDelete = """delete from test_table_projects where id = %s;"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbDelete, str(obj_id))
            cursor.execute("""select * from test_table_projects;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка при удалении проекта")


def delete_list(obj_id, connect=None):
    """
    Удалить список
    Аргументы:
    obj_id -- идентификатор списка
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbDelete = """delete from test_table_lists where id = %s;"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbDelete, str(obj_id))
            cursor.execute("""select * from test_table_lists;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка при удалении списка")


def delete_task(obj_id, connect):
    """Удалить задачу

    Аргументы:
    obj_id -- идентификатор задачи
    """
    # Подготавливаем SQL запрос для передачи его базе данных
    dbDelete = """delete from test_table_tasks where id = %s;"""
    # Пробуем передать запрос на изменение элемента, вывод его в терминал и фиксация его в бд
    try:
        with connect.cursor() as cursor:
            cursor.execute(dbDelete, str(obj_id))
            cursor.execute("""select * from test_table_tasks;""")
            print(cursor.fetchall())
            connect.commit()
    # В случае неудачи, оповещаем о ошибке
    except:
        print("Ошибка")
